package edu.gsu.view;

import com.vaadin.terminal.StreamResource;
import com.vaadin.ui.Audio;

import edu.gsu.domain.TrackBlob;
import edu.gsu.util.AudioUtil;

public class AudioPlayer extends Audio {

	private TrackBlob blob;

	public AudioPlayer() {
		super();
	}

	public void playSong(TrackBlob blob) {
		if (this.blob == blob) {
			pause();
			this.blob = null;
			setSource(null);
		} else {
			this.blob = blob;
			StreamResource resource = AudioUtil.convertBytes2StreamResource(blob.getBytes());
			setSource(resource);
			play();
		}
	}
}
