package edu.gsu.view;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.vaadin.terminal.Sizeable;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.Slider;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import edu.gsu.domain.Track;
import edu.gsu.domain.TrackBlob;

public class MainWindow extends Window implements ClickListener {

	private VerticalLayout rootLayout;
	private HorizontalLayout top;
	private HorizontalSplitPanel bottom;
	private VerticalLayout sidebar;
	private TrackTable listing;
	private UploadComponent uploadComponent;

	// Playback controls
	private HorizontalLayout playback;
	private Button prev;
	private Button play;
	private Button next;

	// Volume controls
	private HorizontalLayout volume;
	private Button mute;
	private Slider vol;
	private Button max;

	private ComboBox search;
	private Button toggleVisualization;
	private HorizontalLayout status;
	private Label timeFromStart;

	// View mode buttons
	private HorizontalLayout viewmodes;
	private Button viewAsTable;
	private Button viewAsGrid;
	private Button coverflow;

	private VerticalLayout trackDetails;
	private Label timeToEnd;
	private NativeButton jumpToTrack;
	private Label track;
	private Label album;
	private Slider progress;
	private VerticalLayout selections;
	private Label library;
	private NativeButton music;
	private Label store;
	private Button vaadinTunesStore;
	private Button purchased;
	private NativeButton genius;
	private NativeButton recent;

	private AudioPlayer audioPlayer;

	public MainWindow() {

		setCaption("VaadinTunes");
		setTheme("vaadintunes");
		setStyleName("tTunes");

		rootLayout = new VerticalLayout();
		rootLayout.setSizeFull();
		rootLayout.setMargin(false);
		setContent(rootLayout);

		
		audioPlayer =  new AudioPlayer();
//		audioPlayer.setVisible(false);
		rootLayout.addComponent(audioPlayer);

		top = buildTopLayout();
		rootLayout.addComponent(top);

		// We'll need one splitpanel to separate the sidebar and track listing
		bottom = buildBottom();
		rootLayout.addComponent(bottom);
		rootLayout.setExpandRatio(bottom, 1.0F);
	}

	private HorizontalLayout buildTopLayout() {
		// Top area, containing playback and volume controls, play status, view
		// modes and search
		top = new HorizontalLayout();
		top.setWidth("100%");
		top.setHeight("75px"); // Same as the background image height
		top.setMargin(false, true, false, true);
		top.setSpacing(true);
		top.setStyleName("top");

		// Create the placeholders for all the components in the top area
		// Add the components and align them properly
		playback = buildPlayback();
		top.addComponent(playback);
		top.setComponentAlignment(playback, Alignment.MIDDLE_LEFT);

		volume = buildVolume();
		top.addComponent(volume);
		top.setComponentAlignment(volume, Alignment.MIDDLE_LEFT);

		status = buildStatus();
		top.addComponent(status);
		top.setComponentAlignment(status, Alignment.MIDDLE_CENTER);
		top.setExpandRatio(status, 1.0F);

		viewmodes = buildViewmodes();
		top.addComponent(viewmodes);
		top.setComponentAlignment(viewmodes, Alignment.MIDDLE_LEFT);

		search = buildSearch();
		top.addComponent(search);
		top.setComponentAlignment(search, Alignment.MIDDLE_LEFT);

		return top;
	}

	private HorizontalLayout buildPlayback() {
		playback = new HorizontalLayout();
		playback.setMargin(false, true, false, false); // Add right-side margin
		playback.setSpacing(true);
		playback.setStyleName("playback");

		prev = new NativeButton("Previous");
		prev.addListener(this);
		prev.setStyleName("prev");
		playback.addComponent(prev);
		playback.setComponentAlignment(prev, Alignment.MIDDLE_LEFT);

		play = new NativeButton("Play/pause");
		play.addListener(this);
		play.setStyleName("play");
		playback.addComponent(play);

		next = new NativeButton("Next");
		next.addListener(this);
		next.setStyleName("next");
		playback.addComponent(next);
		playback.setComponentAlignment(next, Alignment.MIDDLE_LEFT);

		return playback;
	}

	private HorizontalLayout buildVolume() {
		volume = new HorizontalLayout();
		volume.setStyleName("volume");

		mute = new NativeButton("mute");
		mute.addListener(this);
		mute.setStyleName("mute");
		volume.addComponent(mute);

		vol = new Slider();
		vol.setOrientation(Slider.ORIENTATION_HORIZONTAL);
		vol.setWidth("100px");
		vol.setWidth("78px");
		volume.addComponent(vol);

		max = new NativeButton("max");
		max.addListener(this);
		max.setStyleName("max");
		volume.addComponent(max);

		return volume;
	}

	private HorizontalLayout buildStatus() {
		// Status area
		status = new HorizontalLayout();
		status.setWidth("80%");
		status.setHeight("46px"); // Height of the background image
		status.setSpacing(true);
		status.setMargin(true);
		status.setStyleName("status");

		// Place all components to the status layout and align them properly
		toggleVisualization = new NativeButton("Mode");
		toggleVisualization.addListener(this);
		toggleVisualization.setStyleName("toggle-vis");
		status.addComponent(toggleVisualization);
		status.setComponentAlignment(toggleVisualization, Alignment.MIDDLE_LEFT);

		timeFromStart = new Label("0:00");
		status.addComponent(timeFromStart);
		status.setComponentAlignment(timeFromStart, Alignment.BOTTOM_LEFT);

		trackDetails = buildTrackDetails();
		status.addComponent(trackDetails);
		status.setExpandRatio(trackDetails, 1.0F);

		timeToEnd = new Label("-4:46");
		status.addComponent(timeToEnd);
		status.setComponentAlignment(timeToEnd, Alignment.BOTTOM_LEFT);

		jumpToTrack = new NativeButton("Show");
		jumpToTrack.addListener(this);
		jumpToTrack.setStyleName("jump");
		status.addComponent(jumpToTrack);
		status.setComponentAlignment(jumpToTrack, Alignment.MIDDLE_LEFT);

		return status;
	}

	private VerticalLayout buildTrackDetails() {
		// We'll need another layout to show currently playing track and
		// progress
		trackDetails = new VerticalLayout();
		trackDetails.setWidth("100%");

		track = new Label("Track Name");
		track.setWidth(null);
		trackDetails.addComponent(track);
		trackDetails.setComponentAlignment(track, Alignment.TOP_CENTER);

		album = new Label("Album Name - Artist");
		album.setWidth(null);
		trackDetails.addComponent(album);
		trackDetails.setComponentAlignment(album, Alignment.TOP_CENTER);

		progress = new Slider();
		progress.setOrientation(Slider.ORIENTATION_HORIZONTAL);
		progress.setWidth("100%");
		trackDetails.addComponent(progress);

		return trackDetails;
	}

	private HorizontalLayout buildViewmodes() {
		viewmodes = new HorizontalLayout();

		viewAsTable = new NativeButton("Table");
		viewAsTable.setStyleName("viewmode-table");
		viewmodes.addComponent(viewAsTable);

		viewAsGrid = new NativeButton("Grid");
		viewAsGrid.setStyleName("viewmode-grid");
		viewmodes.addComponent(viewAsGrid);

		coverflow = new NativeButton("Coverflow");
		coverflow.setStyleName("viewmode-coverflow");
		viewmodes.addComponent(coverflow);

		return viewmodes;
	}

	private ComboBox buildSearch() {
		search = new ComboBox();
		return search;
	}

	private HorizontalSplitPanel buildBottom() {
		bottom = new HorizontalSplitPanel();
		// Give the sidebar less space than the listing
		bottom.setSplitPosition(200, Sizeable.UNITS_PIXELS);

		// Let's add some content to the sidebar
		// First, we need a layout to but all components in
		sidebar = buildSideBar();
		bottom.setFirstComponent(sidebar);

		// Then comes the cover artwork (we'll add the actual image in the
		// themeing section)
		Embedded cover = new Embedded("Currently Playing");
		cover.setSource(new ThemeResource("images/album-cover.jpg"));
		cover.setWidth("100%");
		sidebar.addComponent(cover);

		/*
		 * And lastly, we need the track listing table It should fill the whole
		 * left side of our bottom layout
		 */
		VerticalLayout rightLayout = new VerticalLayout();
		rightLayout.setSizeFull();
		rightLayout.setSpacing(true);
		bottom.setSecondComponent(rightLayout);

		listing = new TrackTable();
		rightLayout.addComponent(listing);
		rightLayout.setExpandRatio(listing, 1.0f);

		uploadComponent = new UploadComponent();
//		uploadComponent.setHeight("15px");
		rightLayout.addComponent(uploadComponent);

		return bottom;
	}

	private VerticalLayout buildSideBar() {
		sidebar = new VerticalLayout();
		sidebar.setSizeFull();
		sidebar.setStyleName("sidebar");

		/*
		 * Then we need some labels and buttons, and an album cover image The
		 * labels and buttons go into their own vertical layout, since we want
		 * the 'sidebar' layout to be expanding (cover image in the bottom).
		 * VerticalLayout is by default 100% wide.
		 */
		selections = buildSelections();
		sidebar.addComponent(selections);
		// Then add the selections to the sidebar, and set it expanding
		sidebar.setExpandRatio(selections, 1.0F);

		return sidebar;
	}

	private VerticalLayout buildSelections() {
		selections = new VerticalLayout();

		library = new Label("Library");
		selections.addComponent(library);

		music = new NativeButton("Music");
		music.addListener(this);
		music.setWidth("100%");
		music.setStyleName("selected");
		selections.addComponent(music);

		store = new Label("Store");
		selections.addComponent(store);

		vaadinTunesStore = new NativeButton("VaadinTunes Store");
		vaadinTunesStore.addListener(this);
		vaadinTunesStore.setWidth("100%");
		selections.addComponent(vaadinTunesStore);

		purchased = new NativeButton("Purchased");
		purchased.addListener(this);
		purchased.setWidth("100%");
		selections.addComponent(purchased);

		Label playlists = new Label("Playlists");
		selections.addComponent(playlists);

		genius = new NativeButton("Geniues");
		genius.addListener(this);
		genius.setWidth("100%");
		selections.addComponent(genius);

		recent = new NativeButton("Recently Added");
		recent.addListener(this);
		recent.setWidth("100%");
		selections.addComponent(recent);

		return selections;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button source = event.getButton();
		if (source == play) {
			Track selectedTrack = listing.getSelectedValue();
			if (selectedTrack == null)
				return;
			audioPlayer.playSong(selectedTrack.getBlob());
		}
	}
}
