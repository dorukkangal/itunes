package edu.gsu.view;

import java.util.List;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Table;

import edu.gsu.domain.Track;
import edu.gsu.service.TrackService;
import edu.gsu.util.AppContext;

public class TrackTable extends CustomComponent {

	private Table trackTable;
	private BeanItemContainer<Track> trackContainer;

	private TrackService trackService;

	public TrackTable() {
		trackTable = new Table();
		trackTable.setSizeFull();
		trackTable.setSelectable(true);
		setCompositionRoot(trackTable);
		setSizeFull();

		trackContainer = new BeanItemContainer<Track>(Track.class);
		trackTable.setContainerDataSource(trackContainer);
	}

	@Override
	public void attach() {
		super.attach();
		refresh();
	}

	public void refresh() {
		trackService = AppContext.getTrackService();
		List<Track> userTracks = trackService.findAll();
		trackContainer.removeAllItems();
		trackContainer.addAll(userTracks);
	}

	public Track getSelectedValue() {
		return (Track) trackTable.getValue();
	}
}
