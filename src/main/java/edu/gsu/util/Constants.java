package edu.gsu.util;

import java.util.Arrays;
import java.util.List;

public interface Constants {

	public static final String MIMETYPE_MP3 = "audio/mp3";
	public static final List<String> ACCEPTED_TRACK_MIMETYPES = Arrays.asList(MIMETYPE_MP3, "video/avi");
}
