package edu.gsu.session;

import com.vaadin.Application;

import edu.gsu.domain.User;

public class UserSession {

	private static UserSessionObject sessionObject;

	static {
		sessionObject = new UserSessionObject();
	}

	public static Application getApp() {
		return sessionObject.getApplication();
	}

	public static void setApp(Application app) {
		sessionObject.setApplication(app);
	}

	public static User getUser() {
		return sessionObject.getUser();
	}

	public static void setUser(User user) {
		sessionObject.setUser(user);
	}
}
