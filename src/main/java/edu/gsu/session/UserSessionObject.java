package edu.gsu.session;

import com.vaadin.Application;

import edu.gsu.domain.User;

public class UserSessionObject {

	private User user = null;

	private Application application;

	UserSessionObject() {

	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	User getUser() {
		return user;
	}

	void setUser(User user) {
		this.user = user;
	}
}
