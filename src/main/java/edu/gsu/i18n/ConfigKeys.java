package edu.gsu.i18n;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ConfigKeys {

	public static final String UPLOAD_DIR = "upload.temp.dir";
	public static final String MAX_UPLOAD_SIZE = "max.upload.size";
	public static final String MAIL_USER_ID = "mail.sender";
	public static final String MAIL_HOST = "mail.host";
	public static final String MAIL_PORT = "mail.port";
	public static final String MAIL_USER_PASSWORD = "mail.password";
	public static final String RECEIVER_FIRSTNAME = "{!Receiver.FirstName}";
	public static final String SENDER_FIRSTNAME = "{!Sender.FirstName}";
	public static final String ORG_NAME = "{!Organization.Name}";
	public static List<String> templateVariablesList = new ArrayList<String>();

	static {
		templateVariablesList.add(RECEIVER_FIRSTNAME);
		templateVariablesList.add(SENDER_FIRSTNAME);
		templateVariablesList.add(ORG_NAME);
	}

	public String getKey(String key) {
		try {
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("GlobalConfig.properties");
			Properties properties = new Properties();
			properties.load(inputStream);
			return properties.getProperty(key);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
