package edu.gsu.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

public class Messages {

	private static String DEFAULT_BUNDLE_NAME = "messages";

	private static ResourceBundle bundle;

	public static void initBundle(String bundleName, Locale locale) {
		try {
			bundle = ResourceBundle.getBundle(bundleName, locale);
		} catch (Exception e) {
			initDefaultBundle();
		}
	}

	public static void initDefaultBundle() {
		bundle = ResourceBundle.getBundle(DEFAULT_BUNDLE_NAME, Locale.ENGLISH);
	}

	public static String getKey(String key) {
		return bundle.getString(key);
	}
}
