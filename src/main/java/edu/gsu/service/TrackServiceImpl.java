package edu.gsu.service;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.springframework.stereotype.Service;

import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;

import edu.gsu.domain.Track;
import edu.gsu.domain.TrackBlob;

@Service
public class TrackServiceImpl extends BaseServiceImpl<Track> implements TrackService {

	public TrackServiceImpl() {
		super(Track.class);
	}

	public void saveTrack(File trackFile) throws UnsupportedAudioFileException, IOException {
		AudioInputStream audioIStream = AudioSystem.getAudioInputStream(trackFile);
		byte[] audioBytes = new byte[audioIStream.available()];
		audioIStream.read(audioBytes);

		TrackBlob trackBlob = new TrackBlob();
		trackBlob.setBytes(audioBytes);

		Track track = new Track();
		track.setBlob(trackBlob);

		Mp3File song = null;
		try {
			song = new Mp3File(trackFile);
			if (song.hasId3v2Tag()) {
				ID3v2 id3v2tag = song.getId3v2Tag();
				String album = id3v2tag.getAlbum();
				String artist = id3v2tag.getArtist();
				String title = id3v2tag.getTitle();
				String genreDesc = id3v2tag.getGenreDescription();

				track.setAlbum(album);
				track.setArtist(artist);
				track.setGenre(genreDesc);
				track.setName(title);
			}
		} catch (UnsupportedTagException | InvalidDataException | IOException e) {
			e.printStackTrace();
		}
		super.save(track);
	}
}
