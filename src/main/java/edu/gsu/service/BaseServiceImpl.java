package edu.gsu.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import edu.gsu.domain.BaseEntity;

public abstract class BaseServiceImpl<T extends BaseEntity> implements BaseService<T> {

	private static final Log LOG = LogFactory.getLog(BaseService.class);

	@PersistenceContext
	private EntityManager em;

	private Class<T> entityClass;

	public BaseServiceImpl(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		LOG.debug("Find all " + entityClass.getSimpleName());
		return em.createQuery("from " + entityClass.getSimpleName()).getResultList();
	}

	@SuppressWarnings("unchecked")
	public T find(Integer id) {
		try {
			LOG.debug("find by id " + id + entityClass.getSimpleName());
			return (T) em.createQuery("select c from UserBroadcast c where c.id =?").setParameter(1, id).getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}

	@Transactional
	public void save(T entity) {
		LOG.debug("Save entity " + entityClass.getSimpleName());
		em.persist(entity);
	}

	@Transactional
	public void update(T entity) {
		LOG.debug("Update entity " + entityClass.getSimpleName());
		em.merge(entity);
	}

	@Transactional
	public void delete(T entity) {
		LOG.debug("Delete entity " + entityClass.getSimpleName());
		em.remove(entity);
	}

	@Transactional
	public void refresh(T entity) {
		LOG.debug("Refresh entity " + entityClass.getSimpleName());
		em.refresh(entity);
	}

	@Transactional
	public void flush() {
		LOG.debug("Flush entity manager");
		em.flush();
	}
}
