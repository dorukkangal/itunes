package edu.gsu.service;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.UnsupportedAudioFileException;

import edu.gsu.domain.Track;

public interface TrackService extends BaseService<Track> {

	public void saveTrack(File trackFile) throws UnsupportedAudioFileException, IOException;
}