package edu.gsu.service;

import java.util.List;

import edu.gsu.domain.BaseEntity;

public interface BaseService<T extends BaseEntity> {

	public List<T> findAll();

	public T find(Integer id);

	public void save(T entity);

	public void update(T entity);

	public void delete(T entity);

	public void refresh(T entity);

	public void flush();
}
