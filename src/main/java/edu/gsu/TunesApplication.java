package edu.gsu;

import com.vaadin.Application;
import com.vaadin.ui.Audio;

import edu.gsu.i18n.Messages;
import edu.gsu.session.UserSession;
import edu.gsu.view.MainWindow;

public class TunesApplication extends Application {
	private static final long serialVersionUID = 1L;

	private MainWindow mainWindow;

	public Audio audio;

	@Override
	public void init() {
		UserSession.setApp(this);
		initBundle();

		mainWindow = new MainWindow();
		setMainWindow(mainWindow);
	}

	private void initBundle() {
		Messages.initDefaultBundle();
	}
}