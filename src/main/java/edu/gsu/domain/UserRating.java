package edu.gsu.domain;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class UserRating extends BaseEntity {
	private int rating;

	@OneToOne
	private User user;

	@OneToOne
	private Track track;

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}
}
