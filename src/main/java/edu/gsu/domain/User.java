package edu.gsu.domain;

import javax.persistence.Entity;

@Entity
public class User extends BaseEntity {

	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
