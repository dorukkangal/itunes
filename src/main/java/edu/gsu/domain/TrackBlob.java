package edu.gsu.domain;

import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
public class TrackBlob extends BaseEntity {

	@Lob
	private byte[] bytes;

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
}
